#!/usr/bin/env python
'''
Created on 9 Dec 2013

@author: Akul Mathur
'''

from gevent import socket

#import socket
import select
import ujson
from collections import defaultdict


from joblib import Parallel, delayed

host = '' or socket.gethostname()
port = 5001
backlog = 5 
size = 102400