'''
Created on 9 Dec 2013

@author: Akul Mathur
'''

from __init__ import socket, host, port, size
from __init__ import ujson
from __init__ import Parallel, delayed
from __init__ import select

from_server = []
to_server = []

def recv_server_to_client(s,size):
    data = None
    try:
        data = s.recv(size)
        print '\n Received:', data
    except (socket.error), e: 
        print e.args,e.message
    return data


if __name__ == "__main__":
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #s.setblocking(False)
    s.connect((host, port))
    from_server += [s]
    to_server += [s]
    data = recv_server_to_client(s,size)
    if data is not None:
        data = ujson.loads(data)
        chooser = (raw_input("Choose any clients to chat with \n {}".format(data)))
        s.send(ujson.dumps({chooser:data[chooser]}))
        data = recv_server_to_client(s,size)
        print 'Received: {}'.format(data)
    while True:
        msg = raw_input('\n Enter some msg ya\'ll: ')
        if msg == chr(27):
            s.send('None')
            s.close()
            del s
            break;
        try:
            incoming, outgoing,errorlist = select.select(from_server, to_server, [])
            for out_req in outgoing:
                if out_req == s:
                    out_req.send(msg)
            for inc_req in incoming:
                if inc_req == s:
                    print recv_server_to_client(inc_req,size)
        except (select.error) as e: print e.args,e.message