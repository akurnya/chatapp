'''
Created on 9 Dec 2013

@author: Asus s
'''
from gevent import monkey
monkey.patch_all()
import gevent

from __init__ import socket, host, port, size, backlog
from __init__ import select
from __init__ import ujson
from __init__ import defaultdict
#from __init__ import Parallel, delayed

clientlist = defaultdict()
to_client = []
    
    
def run(client, address):
    #this is what accepts and creates a dedicated client socket per socket
    #client.settimeout(2.0)
    data = None
    try:
        data = client.recv(size)
        #Get the selected client ip to contact to
        if data or 0 or None:
            try:
                data = ujson.loads(data)
                if isinstance(data,dict):
                    #data in format: {chooser:data[chooser]}
                    number,addr = data.items()[0]
                    if addr in clientlist.values():
                        client.send('True')
                    else:
                        client.send('Client disconnected from us')
            except (ValueError) as e:
                if data == 'None':
                    client.close()
                    to_client.remove(client)
                    try:
                        clientlist.pop(client)
                    except (KeyError) as e: print e.args,e.message
                else:
                    print "Client sent {} | Server sending data to client address {}".format(data, address)
                    your_msg = raw_input('\n Enter some msg ya\'ll: ')
                    client.send(your_msg)
                    #data = client.recv(size)
        else: pass
    except (socket.error), e: 
        #client.close()
        print e.args, e.message
        client.close()


def recv_from_server(srvr, to_client):
    client, address = srvr.accept()
    clientlist[client] = address
    to_client += [client]
    client.send(ujson.dumps(dict(zip(xrange(len(clientlist.values())),clientlist.values()))))

def send_to_client(client):
    run(client, clientlist[client])


def server_runner(server):
    while True:
        incoming_client,outgoing_client,error_socket = select.select([server],to_client,[])
        list((recv_from_server)(srvr, to_client) for srvr in incoming_client)
        list((send_to_client)(client) for client in outgoing_client)


if __name__ == '__main__':
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setblocking(False)
    server.bind((host, port)) 
    server.listen(backlog)
    #run(server)
    (server_runner)(server)